# irstea/composer-require-checker-shim - shim repository for maglnet/composer-require-checker.

This package is a drop-in replacement for [maglnet/composer-require-checker](https://github.com/maglnet/ComposerRequireChecker), which provides its PHAR archive as a binary.

It is built automatically from the official PHAR.

## Installation

	composer require irstea/composer-require-checker-shim

or:

	composer require --dev irstea/composer-require-checker-shim



## Usage

As you would use the original package, i.e. something like:

	vendor/bin/composer-require-checker [options] [arguments]

## License

This distribution retains the license of the original software: MIT